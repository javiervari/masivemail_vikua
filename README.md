# README

## 1. Install requirements

```
pip install -r requirements.txt
```

## 2. Set the mail recipients in ```./mailObjects/contacts.txt``` file in the format __name__ __email__

>EXAMPLE:
Juan Juan@mail.com 
Pedro Pedro@mail.com
Laura Laura@mail.com 

## 3. In ```mail.py```, in the variables MYUSER and PASS, set the credentials of mail owner

## 4. Add data in ```./data/``` folder with name "resumenDiario.csv"

>For Example, please view EXAMPLE.csv

## 5. The reports will be saved and storaged in ```./reports/``` 

## IMPORTANT: Dont't change the PNG images storaged in ```./img/```, otherwise, all the variables in the report are out of line




*by Javier Valero*
