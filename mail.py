#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from PIL import Image, ImageDraw, ImageFont
from datetime import datetime, timedelta
import pandas as pd

MYUSER = "jvalero@vikua.com"
PASS = "26772677vikua"

def format_fix_position_item(item, to_do):
	""" Format or Position some items of report
	"""

	if to_do == "format": #Format
		if "PromSinReportar" in item: #build a timedelta obj and return in format "XX min XX seg"
			dato = item["PromSinReportar"].replace(",",".")
			dato = str(timedelta(seconds=float(dato))).split(":")
			dato = dato[1]+" min "+(str(int(float(dato[2]))))+" seg"
			return str(dato)

		elif "NumeroMensajes" in item and item["NumeroMensajes"]>1000: #format NumeroMensajes, if > 1000 then /1000 and add "K"
			dato = item["NumeroMensajes"]/1000
			dato = str(round(dato,1)) + "K"
			return dato

		elif "VelocidadPromedio" in item: #Rounded to 2 places and add " km/h"
			dato = float(item["VelocidadPromedio"].replace(",","."))
			dato = round(dato,2)
			return str(dato) + " km/h"


		elif "DistanciaRecorrida" in item:
			try:
				dato = item["DistanciaRecorrida"].replace(",",".")
				dato = float(dato)
			except:
				dato = float(item["DistanciaRecorrida"])
			return str(round(dato))

		elif "HorasTrabajo" in item: #Returns rounded hour and integer
			try:
				dato = item["HorasTrabajo"].replace(",",".")
				dato = float(dato)
			except:
				dato = float(item["HorasTrabajo"])

			dato = str(timedelta(hours=dato)).split(":")

			if int(dato[1]) >= 30:
				dato = str(int(dato[0])+1)
				return dato
			else:
				dato = dato[0]
				return dato

		elif "VelocidadMaxima" in item:
			try:
				dato = item["VelocidadMaxima"].replace(",",".")
				dato = float(dato)
			except:
				dato = float(item["VelocidadMaxima"])

			return str(round(dato))+" km/h"





	else: #position
		if "HorasTrabajo" in item: #Returns the position in X in function of the hour
			try:
				dato = item["HorasTrabajo"].replace(",",".")
				dato = float(dato)
			except:
				dato = float(item["HorasTrabajo"])

			dato = str(timedelta(hours=dato)).split(":")

			if int(dato[0]) >= 10:
				return (649, 530)
			else:
				return (666, 530)


		elif "DistanciaRecorrida" in item: #Returns the position in X in function of the DistanciaRecorrida
			try:
				dato = item["DistanciaRecorrida"].replace(",",".")
				dato = float(dato)
			except:
				dato = float(item["DistanciaRecorrida"])

			dato = round(dato)

			if dato < 10:
				return(669, 1195)
			elif dato < 100:
				return(649, 1195)
			else:
				return(628, 1195)



def create_reports():
	""" Creates the reports, 
		aggregates the info provides from ./data/resumenDiario.csv, 
		draw over the ./img/Reporte-JTEP-B.png image, 
		save the report as PDF
		and return its path in a dictionary {"PATH" : "NAME REPORT"}
	"""
	all_reports_created = {}

	data_path = "./data/resumenDiario.csv"

	font_type = ImageFont.truetype("/usr/share/fonts/truetype/freefont/FreeSans.ttf", 25)

	#font distancia y horatrabajo
	font_type_distancia_horastrabajo = ImageFont.truetype("/usr/share/fonts/truetype/freefont/FreeSansBold.ttf", 68)

	#font idvehicle
	font_type_idvehiculo = ImageFont.truetype("/usr/share/fonts/truetype/freefont/FreeSans.ttf", 37)

	df = pd.read_csv(data_path, sep=";")

	for fil, col in df.iterrows():
		folder = "./reports/" 
		filename = "ID"+str(col["Vehiculo"]) + "-" + str(datetime.now().strftime("%Y-%m-%d"))+".pdf"
		report_path = folder+filename

		img = Image.open("./img/Reporte-JTEP-B.png") #Image instance
		draw = ImageDraw.Draw(img)
		
		draw.text(xy=(619, 179),
				  text = "Fecha: " + str(datetime.strptime(col["Fecha"],"%Y-%m-%d").strftime("%d/%m/%Y")),  
				  fill=(11, 86, 141),
				  font=font_type) #Fecha de reporte

		draw.text(xy=(470, 333), 
				  text="ID:"+str(col["Vehiculo"]), 
				  fill=(255,255,255), 
				  font=font_type_idvehiculo) #id vehiculo


		draw.text(xy=(131, 549), 
				  text=str(datetime.strptime(col["PrimerRegistro"],"%Y-%m-%d %H:%M:%S").strftime("%I:%M:%S %p")), 
				  fill=(255,255,255), 
				  font=font_type) #primer reporte


		draw.text(xy=(203, 670), 
				  text=str(datetime.strptime(col["UltimoRegistro"],"%Y-%m-%d %H:%M:%S").strftime("%I:%M:%S %p")), 
				  fill=(255,255,255), 
				  font=font_type) #ultimo reporte


		draw.text(xy=(131, 803), 
				  text=format_fix_position_item({"PromSinReportar":col["PromSinReportar"]}, "format"), 
				  fill=(255,255,255), 
				  font=font_type) #tiempo promedio sin reportar

		draw.text(xy=(260, 912),
			   	  text=format_fix_position_item({"NumeroMensajes":col["NumeroMensajes"]}, "format"), 
			   	  fill=(255,255,255), 
			   	  font=font_type) #numero de mensajes


		draw.text(xy=(131, 1065), 
				  text=format_fix_position_item({"VelocidadMaxima":col["VelocidadMaxima"]}, "format"), 
				  fill=(255,255,255), 
				  font=font_type) #velocidad max


		draw.text(xy=(203, 1170), 
				  text=format_fix_position_item({"VelocidadPromedio":col["VelocidadPromedio"]}, "format"), 
				  fill=(255,255,255), 
				  font=font_type) #velocidad prom


		draw.text(xy=format_fix_position_item({"HorasTrabajo":col["HorasTrabajo"]}, "position"), 
				  text=format_fix_position_item({"HorasTrabajo":col["HorasTrabajo"]}, "format"),
				  fill=(0,0,0), 
				  font=font_type_distancia_horastrabajo) #horas de trabajo



		draw.text(xy=format_fix_position_item({"DistanciaRecorrida":col["DistanciaRecorrida"]}, "position"),    
			      text=format_fix_position_item({"DistanciaRecorrida":col["DistanciaRecorrida"]}, "format"), 
			      fill=(0,0,0), 
			      font=font_type_distancia_horastrabajo) #distancia recorrida



		img.show()
		img.save(report_path)
		print("Reporte de la unidad " + str(col["Vehiculo"]) + " Listo" )
		all_reports_created[report_path] = filename

	return all_reports_created 


def get_contacts(contacts_file):
	"""
	Gets the contacts storaged in .mailObjects/contacts.txt and return it
	"""
	names = []
	emails = []

	with open(contacts_file, mode='r') as contacts_file:
		for el in contacts_file:
			names.append(el.split()[0])
			emails.append(el.split()[1])
	return (names, emails)




def main():
	try:
		print("Generando reportes...")
		all_reports = create_reports() # Create Report and save it
		print("\nUbicacion de reportes:")
		for i in all_reports:
			print(i)
	except Exception as e:
		print("Ocurrió un problema al generar los reportes\n[DETALLE]:", e)
		sys.exit()

	print("\nLeyendo destinatarios...")
	names, emails = get_contacts("./mailObjects/contacts.txt")
	print("Listo\n")

	print("Intentando conectar", MYUSER, "...")
	
	try:
		s = smtplib.SMTP(host='smtp.gmail.com', port=587)#465
		s.starttls()
		s.login(MYUSER, PASS)
		print(MYUSER, "Conectado exitosamente\n")
	except Exception as e:
		print("No se ha podido conectar el usuario", MYUSER)
		print("Sin embargo los reportes se han generado en la ruta especificada")
		print("[DETALLE]:",e)
		sys.exit()

	print("Enviando correos...")
	# for name, email in zip(names, emails):
	# 	# create a message object
	# 	msg = MIMEMultipart()

	# 	# setup the parameters of the report_path
	# 	msg['From']=MYUSER
	# 	msg['To']=email
	# 	msg['Subject']="REPORTE JTEP "+str(datetime.now().strftime("%Y-%m-%d"))
		
	# 	# add in the report_path body
	# 	for r in all_reports:
	# 		pdf_object=open(r,'rb') #read pdf
	# 		attach = MIMEApplication(pdf_object.read(),_subtype="pdf") #instance object of email
	# 		attach.add_header('Content-Disposition', 'attachment', filename=all_reports[r])
	# 		pdf_object.close() #close pdf object
	# 		msg.attach(attach)


	# 	# send the reports via the server set up earlier.
	# 	s.send_message(msg)
	# 	del msg
	# 	print("Email enviado exitosamente a", name, email)

	# # Terminate the SMTP session and close the connection
	# s.quit()
	# print("\nProceso finalizado...")

if __name__ == '__main__':
    main()